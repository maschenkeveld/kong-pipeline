#!/bin/bash

source exports-secrets.sh

export DECK_API_NAME="alice" 
export KONNECT_ADDR=https://eu.api.konghq.com
export KONNECT_CONTROL_PLANE_NAME=default

export WORKDIR=$(pwd)

validate_yes_no() {
    read -p "$1 (y/n): " response
    while [[ ! "$response" =~ ^[YyNn]$ ]]; do
        read -p "Please enter 'y' or 'n': " response
    done
}

validate_numeric() {
    read -p "$1" response
    while [[ ! "$response" =~ ^[1-9][0-9]*$ ]]; do
        read -p "Please enter a valid number: " response
    done
}

# validate_yes_no "Do you want to deploy to Konnect?"
# if [[ $response =~ ^[Yy]$ ]]; then
#     export DEPLOY_TO_KONNECT=true
# else
#     export DEPLOY_TO_KONNECT=false
# fi

# validate_yes_no "Do you want to deploy to Kubernetes (KIC)?"
# if [[ $response =~ ^[Yy]$ ]]; then
#     export DEPLOY_TO_KUBERNETES=true
# else
#     export DEPLOY_TO_KUBERNETES=false
# fi

# validate_yes_no "Do you want to create an API product?"
# if [[ $response =~ ^[Yy]$ ]]; then
#     export CREATE_API_PRODUCT=true
# else
#     export CREATE_API_PRODUCT=false
# fi

mkdir $WORKDIR/apis/$DECK_API_NAME/deck-file
mkdir $WORKDIR/apis/$DECK_API_NAME/deck-file/dumped
mkdir $WORKDIR/apis/$DECK_API_NAME/deck-file/generated

deck file openapi2kong -s $WORKDIR/apis/$DECK_API_NAME/openapi-spec/openapi-spec.yaml > $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-generated.yaml

source $WORKDIR/common/env-vars/$KONNECT_CONTROL_PLANE_NAME
source $WORKDIR/apis/$DECK_API_NAME/env-vars/$KONNECT_CONTROL_PLANE_NAME
cat $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-generated.yaml | deck file patch -s - $WORKDIR/common/patches/patches.yaml | deck file patch -s - $WORKDIR/apis/$DECK_API_NAME/patches/patches.yaml | deck file add-plugins -s - $WORKDIR/apis/$DECK_API_NAME/plugins/plugins.yaml | deck file add-tags -s - $DECK_API_NAME > $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-patched.yaml
cat $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-patched.yaml

deck gateway diff $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-patched.yaml $WORKDIR/common/plugin-templates/opentelemetry.yaml --konnect-addr $KONNECT_ADDR --konnect-control-plane-name $KONNECT_CONTROL_PLANE_NAME --konnect-token $KPAT --select-tag $DECK_API_NAME --select-tag $DECK_TAG_CONFIG_OWNER

# deck gateway sync $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-patched.yaml common/plugin-templates/opentelemetry.yaml --konnect-addr $KONNECT_ADDR --konnect-control-plane-name $KONNECT_CONTROL_PLANE_NAME --konnect-token $KPAT --select-tag $DECK_API_NAME --select-tag $DECK_TAG_CONFIG_OWNER

export KONNECT_CONTROL_PLANE_NAME=kic

source $WORKDIR/apis/$DECK_API_NAME/env-vars/$KONNECT_CONTROL_PLANE_NAME
source $WORKDIR/common/env-vars/$KONNECT_CONTROL_PLANE_NAME
echo $DECK_BACKEND_HOSTNAME
cat $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-generated.yaml | deck file patch -s - $WORKDIR/common/patches/patches.yaml | deck file patch -s - $WORKDIR/apis/$DECK_API_NAME/patches/patches.yaml | deck file add-plugins -s - $WORKDIR/apis/$DECK_API_NAME/plugins/plugins.yaml | deck file add-tags -s - $DECK_API_NAME > $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-patched.yaml
cat $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-patched.yaml

deck file render $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-patched.yaml $WORKDIR/common/plugin-templates/opentelemetry.yaml -o $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-rendered.yaml --populate-env-vars

mkdir $WORKDIR/apis/$DECK_API_NAME/kic-k8s-manifests

deck file kong2kic -s $WORKDIR/apis/$DECK_API_NAME/deck-file/generated/kong-rendered.yaml -o $WORKDIR/apis/$DECK_API_NAME/kic-k8s-manifests/$DECK_API_NAME.yaml

mkdir $WORKDIR/apis/$DECK_API_NAME/helm-chart
cd $WORKDIR/apis/$DECK_API_NAME/helm-chart
helm create $DECK_API_NAME


cd $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates
yq -s '(.kind | downcase) + "-" + .metadata.name' $WORKDIR/apis/$DECK_API_NAME/kic-k8s-manifests/alice.yaml
cd $WORKDIR

generate_helm_values() {
    input_file="$1"
    output_file="$2"

    variables=($(grep -o '{{[^{}]*}}' "$input_file" | sed 's/{{[[:space:]]*\.Values\.\([^[:space:]]*\)[[:space:]]*}}/\1/'))
    values=($(grep -o '# "[^"]*"$' "$input_file" | sed 's/# "\(.*\)"$/\1/'))

    if [ ${#variables[@]} -ne ${#values[@]} ]; then
        echo "Error: Number of variables does not match number of values."
        exit 1
    fi

    for ((i=0; i<${#variables[@]}; i++)); do
        export var=".${variables[$i]}"
        export value="${values[$i]}"
        echo "Setting $var to $value"

        yq -i 'eval(strenv(var)) = strenv(value)' "$WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/values.yaml"

        unset var
        unset value
    done
}

generate_helm_values "$WORKDIR/apis/$DECK_API_NAME/env-vars/$KONNECT_CONTROL_PLANE_NAME" "$WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/values.yaml"

generate_helm_values "$WORKDIR/common/env-vars/$KONNECT_CONTROL_PLANE_NAME" "$WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/values.yaml"

validate_yes_no "Cleanup Helm Chart"
if [[ $response =~ ^[Yy]$ ]]; then
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/deployment.yaml
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/.yml
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/ingress.yaml
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/service.yaml
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/tests
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/NOTES.txt
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/_helpers.tpl
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/hpa.yaml
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME/templates/serviceaccount.yaml
fi

cd $WORKDIR/apis/$DECK_API_NAME

package_output=$(helm package "$WORKDIR/apis/$DECK_API_NAME/helm-chart/$DECK_API_NAME")

echo "$package_output"

chart_filename=$(echo "$package_output" | awk '{print $NF}')

curl -u admin:$MPW https://nexus.pve-1.schenkeveld.io:8443/repository/helm-hosted/ --upload-file $chart_filename

cd $WORKDIR

validate_yes_no "Cleanup?"
if [[ $response =~ ^[Yy]$ ]]; then
    rm -rf $WORKDIR/apis/$DECK_API_NAME/deck-file
    rm -rf $WORKDIR/apis/$DECK_API_NAME/helm-chart
    rm -rf $WORKDIR/apis/$DECK_API_NAME/kic-k8s-manifests
fi

